package com.forando.toyrobot.controller;

import com.forando.toyrobot.controller.entity.RequestRestEntity;
import com.forando.toyrobot.controller.exception.RequestValidationException;
import lombok.experimental.UtilityClass;

@UtilityClass
public class RequestValidator {

    public void validateCommand(RequestRestEntity requestRestEntity) {
        if (requestRestEntity.getCommand() == null) {
            throw new RequestValidationException("[command] value missing");
        }
    }

    public void validateState(RequestRestEntity requestRestEntity) {
        if (requestRestEntity.getFacing() == null) {
            throw new RequestValidationException("[facing] value missing");
        }

        if (requestRestEntity.getX() == null) {
            throw new RequestValidationException("[x] value missing");
        }

        if (requestRestEntity.getY() == null) {
            throw new RequestValidationException("[y] value missing");
        }
    }
}
