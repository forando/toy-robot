package com.forando.toyrobot.controller.entity;

public enum Command {
    PLACE, REPORT, MOVE, LEFT, RIGHT
}
