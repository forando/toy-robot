package com.forando.toyrobot.controller.entity;

import com.forando.toyrobot.domain.dto.Facing;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestRestEntity {

    private Command command;
    private Integer x;
    private Integer y;
    private Facing facing;
}
