package com.forando.toyrobot.controller.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class GameValidationError extends GameSubError {
    private String object;
    private String field;
    private Object rejectedValue;
    private String message;

    GameValidationError(String object, String message) {
        this.object = object;
        this.message = message;
    }
}
