package com.forando.toyrobot.controller.exception;

import com.forando.toyrobot.exception.PositionOutOfBoundsException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        String error = "Malformed JSON request";
        return buildResponseEntity(new GameError(HttpStatus.BAD_REQUEST, error, ex));
    }

    private ResponseEntity<Object> buildResponseEntity(GameError gameError) {
        return new ResponseEntity<>(gameError, gameError.getStatus());
    }

    @ExceptionHandler(RequestValidationException.class)
    protected ResponseEntity<Object> handleEntityNotFound(RequestValidationException ex) {
        String error = "Malformed JSON request";
        GameError gameError = new GameError(HttpStatus.BAD_REQUEST, error, ex);
        return buildResponseEntity(gameError);
    }

    @ExceptionHandler(PositionOutOfBoundsException.class)
    protected ResponseEntity<Object> handleOutOfBounds(PositionOutOfBoundsException ex) {
        String error = "invalid input";
        GameError gameError = new GameError(HttpStatus.FORBIDDEN, error, ex);
        return buildResponseEntity(gameError);
    }

}
