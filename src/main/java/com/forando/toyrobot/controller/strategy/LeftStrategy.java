package com.forando.toyrobot.controller.strategy;

import com.forando.toyrobot.controller.entity.RequestRestEntity;
import com.forando.toyrobot.service.GameService;
import org.springframework.http.ResponseEntity;

public class LeftStrategy implements Strategy {

    @Override
    public ResponseEntity<?> execute(GameService service, RequestRestEntity ignored) {
        service.left();
        return ResponseEntity.noContent().build();
    }
}
