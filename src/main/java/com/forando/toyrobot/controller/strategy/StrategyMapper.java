package com.forando.toyrobot.controller.strategy;

import com.forando.toyrobot.controller.entity.Command;
import lombok.experimental.UtilityClass;

@UtilityClass
public class StrategyMapper {

    public Strategy fromCommand(Command command) {

        switch (command) {
            case PLACE:
                return new PlaceStrategy();
            case LEFT:
                return new LeftStrategy();
            case RIGHT:
                return new RightStrategy();
            case MOVE:
                return new MoveStrategy();
            case REPORT:
            default:
                return new ReportStrategy();
        }
    }
}
