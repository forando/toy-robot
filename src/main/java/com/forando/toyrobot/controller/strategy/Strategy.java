package com.forando.toyrobot.controller.strategy;

import com.forando.toyrobot.controller.entity.RequestRestEntity;
import com.forando.toyrobot.service.GameService;
import org.springframework.http.ResponseEntity;

public interface Strategy {

    ResponseEntity<?> execute(GameService service, RequestRestEntity request);
}
