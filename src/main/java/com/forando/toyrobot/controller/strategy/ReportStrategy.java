package com.forando.toyrobot.controller.strategy;

import com.forando.toyrobot.controller.entity.PositionRestEntity;
import com.forando.toyrobot.controller.entity.RequestRestEntity;
import com.forando.toyrobot.controller.entity.StateRestEntity;
import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.service.GameService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ReportStrategy implements Strategy {

    @Override
    public ResponseEntity<?> execute(GameService service, RequestRestEntity ignored) {
        RobotState report = service.report();
        return new ResponseEntity<>(toRestEntity(report), HttpStatus.OK);
    }

    private StateRestEntity toRestEntity(RobotState state) {
        return new StateRestEntity(
                new PositionRestEntity(state.getPosition().getX(), state.getPosition().getY()), state.getFacing()
        );
    }

}
