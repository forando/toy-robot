package com.forando.toyrobot.controller.strategy;

import com.forando.toyrobot.controller.entity.RequestRestEntity;
import com.forando.toyrobot.service.GameService;
import org.springframework.http.ResponseEntity;

public class RightStrategy implements Strategy {

    @Override
    public ResponseEntity<?> execute(GameService service, RequestRestEntity ignored) {
        service.right();
        return ResponseEntity.noContent().build();
    }
}
