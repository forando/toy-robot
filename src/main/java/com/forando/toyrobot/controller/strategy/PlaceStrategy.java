package com.forando.toyrobot.controller.strategy;

import com.forando.toyrobot.controller.RequestValidator;
import com.forando.toyrobot.controller.entity.RequestRestEntity;
import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.service.GameService;
import org.springframework.http.ResponseEntity;

public class PlaceStrategy implements Strategy {

    @Override
    public ResponseEntity<?> execute(GameService service, RequestRestEntity request) {
        RequestValidator.validateState(request);
        service.place(toRobotState(request));
        return ResponseEntity.noContent().build();
    }

    private RobotState toRobotState(RequestRestEntity request) {
        return new RobotState(new Position(request.getX(), request.getY()), request.getFacing());
    }
}
