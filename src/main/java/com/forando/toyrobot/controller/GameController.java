package com.forando.toyrobot.controller;

import com.forando.toyrobot.controller.entity.RequestRestEntity;
import com.forando.toyrobot.controller.strategy.Strategy;
import com.forando.toyrobot.controller.strategy.StrategyMapper;
import com.forando.toyrobot.service.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class GameController {

    private final GameService service;

    @RequestMapping(value = "/toy-robot", method = RequestMethod.POST)
    public ResponseEntity<?> report(@RequestBody RequestRestEntity request) {

        RequestValidator.validateCommand(request);
        Strategy strategy = StrategyMapper.fromCommand(request.getCommand());
        return strategy.execute(service, request);
    }
}
