package com.forando.toyrobot;

import com.forando.toyrobot.domain.gps.transiver.GpsSatellite;
import com.forando.toyrobot.domain.robot.SingleStepForwardDirectionRobot;
import com.forando.toyrobot.domain.robot.SingleStepRobot;
import com.forando.toyrobot.domain.space.SquareTable;
import com.forando.toyrobot.domain.space.Table;
import com.forando.toyrobot.domain.space.TableBounds;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ToyRobotApplication {

    @Value( "${table.width}" )
    private Integer tableWidth;

    @Value( "${table.length}" )
    private Integer tableLength;

    public static void main(String[] args) {
        SpringApplication.run(ToyRobotApplication.class, args);
    }

    @Bean
    public TableBounds getBounds() {
        return new TableBounds(0, 0, tableWidth, tableLength);
    }

    @Bean
    public Table getTable() {
        return new SquareTable(getBounds());
    }

    @Bean
    public GpsSatellite getGpsSatellite() {
        return  new GpsSatellite(getBounds());
    }

    @Bean
    public SingleStepRobot getSingleStepRobot() {
        return new SingleStepForwardDirectionRobot();
    }
}
