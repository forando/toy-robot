package com.forando.toyrobot.exception;

public class BehaviorNotImplementedException extends Exception {
    public BehaviorNotImplementedException(String message) {
        super(message);
    }
}
