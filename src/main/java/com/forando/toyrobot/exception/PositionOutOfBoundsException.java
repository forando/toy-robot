package com.forando.toyrobot.exception;

public class PositionOutOfBoundsException extends RuntimeException {
    public PositionOutOfBoundsException(String message) {
        super(message);
    }
}
