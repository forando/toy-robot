package com.forando.toyrobot.domain.dto;

import lombok.Data;

@Data
public final class Position {
    private final int x;
    private final int y;
}
