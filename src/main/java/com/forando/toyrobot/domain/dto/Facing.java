package com.forando.toyrobot.domain.dto;

public enum Facing {
    NORTH, WEST, SOUTH, EAST
}
