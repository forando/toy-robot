package com.forando.toyrobot.domain.dto;

import lombok.Data;

@Data
public final class RobotState {
    private final Position position;
    private final Facing facing;
}
