package com.forando.toyrobot.domain.gps.receiver;

abstract class GpsTrackerSwitcher {
    protected abstract void changeMode(GpsTrackerMode newTracker);
}
