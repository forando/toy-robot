package com.forando.toyrobot.domain.gps.receiver;

import com.forando.toyrobot.domain.dto.Facing;
import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.domain.gps.transiver.ClientTracker;
import com.forando.toyrobot.domain.gps.transiver.GpsConnectionManager;
import com.forando.toyrobot.domain.space.Bounds2D;
import com.forando.toyrobot.exception.ConnectionNotEstablishedException;
import com.forando.toyrobot.exception.NoSuchClientException;

abstract class GpsTrackerMode {

    private GpsConnectionManager connectionManager;

    GpsTrackerMode leftTracker;
    GpsTrackerMode rightTracker;

    protected String subscribe(Position initialPosition) throws ConnectionNotEstablishedException {
        if (connectionManager == null) {
            throw new ConnectionNotEstablishedException();
        }

        return connectionManager.subscribeClient(initialPosition);
    }

    /**
     * Provides current robot position.
     * @param id see {@link GpsConnectionManager#subscribeClient(com.forando.toyrobot.domain.dto.Position)}
     * @return actual client dto.
     */
    public RobotState getState(String id) throws NoSuchClientException {

        return new RobotState(getClient(id).getPosition(), getFacing());
    }

    /**
     * Accomplishes movement with the supplied distance in forward direction.
     * @param id see {@link GpsConnectionManager#subscribeClient(com.forando.toyrobot.domain.dto.Position)}
     * @param steps the travel distance measured in steps (single distance unit)
     * @return actual client dto.
     */
    public RobotState goForward(String id, int steps) {
        return updatePosition(getClient(id), steps);
    }

    /**
     * Accomplishes movement with the supplied distance in backward direction.
     * @param id see {@link GpsConnectionManager#subscribeClient(com.forando.toyrobot.domain.dto.Position)}
     * @param steps the travel distance measured in steps (single distance unit)
     * @return actual client dto.
     */
    public RobotState goBackward(String id, int steps) {
        return updatePosition(getClient(id), -steps);
    }

    /**
     * Calculates distance from the current client position to the client forward facing edge of the bounds.
     * @param id see {@link GpsConnectionManager#subscribeClient(com.forando.toyrobot.domain.dto.Position)}
     * @return the distance (in steps) from the given position to the client forward facing edge of the bounds.
     */
    public int getForwardDistanceToEdge(String id){
        return calculateForwardDistanceToEdge(getClient(id), connectionManager.getObservedGeometryBounds());
    }

    /**
     * Calculates distance from the current client position to the client back facing edge of the bounds.
     * @param id see {@link GpsConnectionManager#subscribeClient(com.forando.toyrobot.domain.dto.Position)}
     * @return the distance (in steps) from the given position to the to the client back facing edge of the bounds.
     */
    public int getBackwardDistanceToEdge(String id){
        return calculateBackwardDistanceToEdge(getClient(id), connectionManager.getObservedGeometryBounds());
    }

    protected void setLeftRelation(GpsTrackerMode left) {
        leftTracker = left;
    }
    protected void setRightRelation(GpsTrackerMode right) {
        rightTracker = right;
    }


    void setConnection(GpsConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public abstract void turnRight(GpsTrackerSwitcher switcher);

    public abstract void turnLeft(GpsTrackerSwitcher switcher);

    protected abstract Facing getFacing();
    protected abstract RobotState updatePosition(ClientTracker client, int steps);
    protected abstract int calculateForwardDistanceToEdge(ClientTracker client, Bounds2D bounds);
    protected abstract int calculateBackwardDistanceToEdge(ClientTracker client, Bounds2D bounds);

    private ClientTracker getClient(String id) {
        ClientTracker client = connectionManager.getClient(id);
        if (client == null) {
            throw new NoSuchClientException("Client with id = " + id + " missing");
        }
        return client;
    }
}
