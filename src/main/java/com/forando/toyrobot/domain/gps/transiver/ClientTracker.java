package com.forando.toyrobot.domain.gps.transiver;

import com.forando.toyrobot.domain.dto.Position;
import lombok.Getter;

/**
 * Created on client connection to the {@link GpsSatellite} and tracks client position.
 */
public class ClientTracker {
    @Getter
    private final String id;
    private int x;
    private int y;

    /**
     * Supposed to be used only by {@link GpsConnectionManager#subscribeClient(Position)}
     * @param initial initial client position coordinates
     */
    ClientTracker(String id, Position initial) {
        this.id = id;
        this.x = initial.getX();
        this.y = initial.getY();
    }

    public Position getPosition() {
        return new Position(x, y);
    }

    public Position appendToX(int value) {
        x += value;
        return getPosition();
    }

    public Position deductFromX(int value) {
        x -= value;
        return getPosition();
    }

    public Position appendToY(int value) {
        y += value;
        return getPosition();
    }

    public Position deductFromY(int value) {
        y -= value;
        return getPosition();
    }
}
