package com.forando.toyrobot.domain.gps.transiver;

import com.forando.toyrobot.domain.space.Bounds2D;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

public final class GpsSatellite {

    @Getter
    @Setter(value = AccessLevel.PACKAGE)
    private GpsConnectionManager connectionManager;


    public GpsSatellite(@NonNull Bounds2D observedGeometryBounds) {
        GpsConnectionManager.injectInto(this);
        this.setObservedGeometryBounds(observedGeometryBounds);
    }

    private void setObservedGeometryBounds(Bounds2D observedGeometryBounds) {
        connectionManager.setObservedGeometryBounds(observedGeometryBounds);
    }
}
