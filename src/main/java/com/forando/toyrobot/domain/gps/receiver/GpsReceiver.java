package com.forando.toyrobot.domain.gps.receiver;

import com.forando.toyrobot.domain.dto.Facing;
import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.domain.gps.transiver.GpsConnectionManager;

public class GpsReceiver extends GpsTrackerSwitcher {

    private String id;

    private final GpsTrackerMode east;
    private final GpsTrackerMode north;
    private final GpsTrackerMode west;
    private final GpsTrackerMode south;

    private GpsTrackerMode tracker;

    protected GpsReceiver() {
        this.east = new GpsTrackerModeEast();
        this.north = new GpsTrackerModeNorth();
        this.west = new GpsTrackerModeWest();
        this.south = new GpsTrackerModeSouth();

        setTrackerRelations(east, north, south);
        setTrackerRelations(north, west, east);
        setTrackerRelations(west, south, north);
        setTrackerRelations(south, east, west);
    }

    private void setTrackerRelations(GpsTrackerMode target, GpsTrackerMode left, GpsTrackerMode right) {
        target.setLeftRelation(left);
        target.setRightRelation(right);
    }

    @Override
    protected void changeMode(GpsTrackerMode newTracker) {
        this.tracker = newTracker;
    }

    private GpsTrackerMode getInitialTrackerMode(Facing facing) {

        GpsTrackerMode mode = null;

        switch (facing) {
            case NORTH:
                mode = north;
                break;
            case WEST:
                mode = west;
                break;
            case SOUTH:
                mode = south;
                break;
            case EAST:
                mode = east;
                break;
        }
        return mode;
    }

    public void setInitialState(RobotState initialState) {
        this.tracker = getInitialTrackerMode(initialState.getFacing());
        this.id = tracker.subscribe(initialState.getPosition());
    }

    public void setConnection(GpsConnectionManager connectionManager) {
        RobotState state = null;
        if (id != null) {
            state = tracker.getState(id);
        }
        east.setConnection(connectionManager);
        north.setConnection(connectionManager);
        west.setConnection(connectionManager);
        south.setConnection(connectionManager);

        if (state == null) {
            return;
        }
        setInitialState(state);
    }



    protected void onTurnLeft() {
        tracker.turnLeft(this);
    }

    protected void onTurnRight() {
        tracker.turnRight(this);
    }

    protected RobotState onGoForward(int steps) {
        return tracker.goForward(id, steps);
    }

    protected RobotState onGoBackward(int steps) {
        return tracker.goBackward(id, steps);
    }

    protected int getForwardDistanceToEdge() {
        return tracker.getForwardDistanceToEdge(id);
    }

    protected int getBackwardDistanceToEdge() {
        return tracker.getBackwardDistanceToEdge(id);
    }

    protected RobotState defineState() {
        if (id == null) {
            return null;
        }
        return tracker.getState(id);
    }
}
