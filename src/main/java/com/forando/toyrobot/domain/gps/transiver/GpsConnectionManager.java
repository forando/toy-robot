package com.forando.toyrobot.domain.gps.transiver;

import com.forando.toyrobot.domain.space.Bounds2D;
import com.forando.toyrobot.domain.dto.Position;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Holds all active connections from different clients.<br>
 * Any client can subscribe/unsubscribe to/from GPS position broadcasting by adding/removing its
 * identity to/from this manager client list.
 */
public final class GpsConnectionManager {

    @Getter
    @Setter(value = AccessLevel.PACKAGE)
    private Bounds2D observedGeometryBounds;
    /**
     * key = robotId
     */
    private Map<String, ClientTracker> clients = new HashMap<>();

    /**
     * Only {@link GpsSatellite} instances are allowed to create this manager<br><br>
     * <b>DISCLAIMER:</b> This constructor can be invoked via reflection though.
     */
    private GpsConnectionManager(){}

    /**
     * Only {@link GpsSatellite} instances are allowed to create this manager<br><br>
     * @param gpsSatellite an instance the connectionManager to be injected in.
     */
    static void injectInto(GpsSatellite gpsSatellite) {
        gpsSatellite.setConnectionManager(new GpsConnectionManager());
    }

    /**
     * Subscribes a client to GPS service that allows GPS in its turn to track clients position.
     * @param initialPosition client initial position coordinates.
     * @return Subscription ID. Must be provided in the future to return the stored {@link ClientTracker}.
     */
    public String subscribeClient(Position initialPosition) {
        String id = generateUUID();
        clients.put(id, new ClientTracker(id, initialPosition));
        return id;
    }

    public ClientTracker unsubscribeClient(String id) {
        return clients.remove(id);
    }

    public ClientTracker getClient(String id) {
        return clients.get(id);
    }

    public int getClientSize() {
        return clients.size();
    }

    private String generateUUID() {
        return UUID.randomUUID().toString();
    }
}
