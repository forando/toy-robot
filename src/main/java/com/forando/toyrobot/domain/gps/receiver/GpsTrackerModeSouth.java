package com.forando.toyrobot.domain.gps.receiver;

import com.forando.toyrobot.domain.dto.Facing;
import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.domain.gps.transiver.ClientTracker;
import com.forando.toyrobot.domain.space.Bounds2D;
import lombok.AccessLevel;
import lombok.Setter;

@Setter(value = AccessLevel.PACKAGE)
final class GpsTrackerModeSouth extends GpsTrackerMode {

    @Override
    protected Facing getFacing() {
        return Facing.SOUTH;
    }

    @Override
    protected RobotState updatePosition(ClientTracker client, int steps) {
        return new RobotState(client.deductFromY(steps), getFacing());
    }

    @Override
    protected int calculateForwardDistanceToEdge(ClientTracker client, Bounds2D bounds) {
        return bounds.getDistanceToSouthEdge(client.getPosition());
    }

    @Override
    protected int calculateBackwardDistanceToEdge(ClientTracker client, Bounds2D bounds) {
        return bounds.getDistanceToNorthEdge(client.getPosition());
    }

    @Override
    public void turnRight(GpsTrackerSwitcher switcher) {
        switcher.changeMode(rightTracker);
    }

    @Override
    public void turnLeft(GpsTrackerSwitcher switcher) {
        switcher.changeMode(leftTracker);
    }
}
