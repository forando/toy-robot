package com.forando.toyrobot.domain.gps.receiver;

import com.forando.toyrobot.domain.dto.Facing;
import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.domain.gps.transiver.ClientTracker;
import com.forando.toyrobot.domain.space.Bounds2D;
import lombok.AccessLevel;
import lombok.Setter;

@Setter(value = AccessLevel.PACKAGE)
final class GpsTrackerModeEast extends GpsTrackerMode {

    @Override
    protected Facing getFacing() {
        return Facing.EAST;
    }

    @Override
    protected RobotState updatePosition(ClientTracker client, int steps) {
        return new RobotState(client.appendToX(steps), getFacing());
    }

    @Override
    protected int calculateForwardDistanceToEdge(ClientTracker client, Bounds2D bounds) {
        return bounds.getDistanceToEastEdge(client.getPosition());
    }

    @Override
    protected int calculateBackwardDistanceToEdge(ClientTracker client, Bounds2D bounds) {
        return bounds.getDistanceToWestEdge(client.getPosition());
    }

    @Override
    public void turnRight(GpsTrackerSwitcher switcher) {
        switcher.changeMode(rightTracker);
    }

    @Override
    public void turnLeft(GpsTrackerSwitcher switcher) {
        switcher.changeMode(leftTracker);
    }
}
