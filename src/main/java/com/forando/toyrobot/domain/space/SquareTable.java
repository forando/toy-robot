package com.forando.toyrobot.domain.space;

import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.exception.PositionOutOfBoundsException;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SquareTable implements Table {

    public final static int DEFAULT_WIDTH = 5; //todo: extract it to application.properties
    public final static int DEFAULT_LENGTH = 5;

    private final TableBounds tableBounds;

    @Override
    public int getWidth() {
        return tableBounds.getWidth();
    }

    @Override
    public int getLength() {
        return tableBounds.getLength();
    }

    @Override
    public Position getStart() {
        return tableBounds.getOrigin();
    }

    @Override
    public Position getEnd() {
        return new Position(
                tableBounds.getStartX() + tableBounds.getWidth(),
                tableBounds.getStartY() + tableBounds.getLength()
        );
    }

    /**
     * Creates a robot and places it according the supplied params
     * @param initialState Object dto after placement
     */
    public boolean canBePlaced(RobotState initialState) throws IndexOutOfBoundsException {
        return tableBounds.isWithinBounds(initialState.getPosition());
    }

    @Override
    public void validatePosition(RobotState state) throws PositionOutOfBoundsException {
        tableBounds.validatePosition(state.getPosition());
    }
}
