package com.forando.toyrobot.domain.space;

import com.forando.toyrobot.domain.dto.Facing;
import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.exception.PositionOutOfBoundsException;

import java.util.Set;

public interface Bounds2D {

    /**
     * Checks that incoming position is within this bounds
     * @param position coordinates to be checked
     * @throws IndexOutOfBoundsException if the supplied position is outside these bounds
     */
    void validatePosition(Position position) throws PositionOutOfBoundsException;

    boolean isWithinBounds(Position position);

    /**
     * Calculates distance from the supplied position to the EAST edge of these bounds.
     * @param position position coordinates the distance will be calculated from.
     * @return the distance (in steps) from the given position to the EAST edge
     * @throws IndexOutOfBoundsException if the supplied position is outside these bounds
     */
    int getDistanceToEastEdge(Position position) throws IndexOutOfBoundsException;

    /**
     * Calculates distance from the supplied position to the NORTH edge of these bounds.
     * @param position position coordinates the distance will be calculated from.
     * @return the distance (in steps) from the given position to the NORTH edge
     * @throws IndexOutOfBoundsException if the supplied position is outside these bounds
     */
    int getDistanceToNorthEdge(Position position) throws IndexOutOfBoundsException;

    /**
     * Calculates distance from the supplied position to the WEST edge of these bounds.
     * @param position position coordinates the distance will be calculated from.
     * @return the distance (in steps) from the given position to the WEST edge
     * @throws IndexOutOfBoundsException if the supplied position is outside these bounds
     */
    int getDistanceToWestEdge(Position position) throws IndexOutOfBoundsException;

    /**
     * Calculates distance from the supplied position to the SOUTH edge of these bounds.
     * @param position position coordinates the distance will be calculated from.
     * @return the distance (in steps) from the given position to the SOUTH edge
     * @throws IndexOutOfBoundsException if the supplied position is outside these bounds
     */
    int getDistanceToSouthEdge(Position position) throws IndexOutOfBoundsException;

    /**
     * @return starting position in x, y coordinates.
     */
    Position getOrigin();

    Set<Facing> getOriginEdges();
}
