package com.forando.toyrobot.domain.space;

import com.forando.toyrobot.domain.dto.Facing;
import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.exception.PositionOutOfBoundsException;
import lombok.Data;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
public class TableBounds implements Bounds2D {
    private final int startX;
    private final int startY;
    private final int width;
    private final int length;

    public TableBounds(int startX, int startY, int width, int length) {
        if (width < 1) {
            throw new IllegalArgumentException("width = " + width + ", must be > 1");
        }
        if (length < 1) {
            throw new IllegalArgumentException("length = " + length + ", must be > 1");
        }
        this.startX = startX;
        this.startY = startY;
        this.width = width;
        this.length = length;
    }

    @Override
    public void validatePosition(Position position) throws PositionOutOfBoundsException {
        if (!isWithinXLimits(position.getX())) {
            throwException("X", position.getX(), startX, startX + width);
        }

        if (!isWithinYLimits(position.getY())) {
            throwException("Y", position.getY(), startY, startY + length);
        }
    }

    @Override
    public boolean isWithinBounds(Position position) {
        return isWithinXLimits(position.getX()) && isWithinYLimits(position.getY());
    }

    @Override
    public int getDistanceToEastEdge(Position position) {
        validatePosition(position);
        return width - position.getX();
    }

    @Override
    public int getDistanceToNorthEdge(Position position) {
        validatePosition(position);
        return length - position.getY();
    }

    @Override
    public int getDistanceToWestEdge(Position position) {
        validatePosition(position);
        return position.getX();
    }

    @Override
    public int getDistanceToSouthEdge(Position position) {
        validatePosition(position);
        return position.getY();
    }

    @Override
    public Position getOrigin() {
        return new Position(startX, startY);
    }

    @Override
    public Set<Facing> getOriginEdges() {
        return Stream.of(Facing.WEST, Facing.SOUTH).collect(Collectors.toSet());
    }

    private boolean isWithinXLimits(int newX) {
        return newX >= startX && newX <= startX + width;
    }

    private boolean isWithinYLimits(int newY) {
        return newY >= startY && newY <= startY + length;
    }

    private void throwException(String name, int value, int startBound, int endBound) {
        throw new PositionOutOfBoundsException("[" + name + " = " + value + "]" +
                " coordinate is not in table dimension bounds [" + startBound + "-" + endBound + "]");
    }
}
