package com.forando.toyrobot.domain.space;

import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.exception.PositionOutOfBoundsException;

public interface Table {

    int getWidth();

    int getLength();

    Position getStart();

    Position getEnd();

    boolean canBePlaced(RobotState initialState);

    void validatePosition(RobotState state) throws PositionOutOfBoundsException;
}
