package com.forando.toyrobot.domain.robot;

import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.domain.gps.transiver.GpsConnectionManager;
import com.forando.toyrobot.exception.BehaviorNotImplementedException;
import com.forando.toyrobot.exception.ConnectionNotEstablishedException;

/**
 * This robot can move only at a distance equal 1 step
 */
public interface SingleStepRobot {

    void turnLeft();

    void turnRight();

    void stepForward();

    void stepBackward() throws BehaviorNotImplementedException;

    RobotState getState();

    void setInitialState(RobotState initialState)throws ConnectionNotEstablishedException;

    void setConnection(GpsConnectionManager connectionManager);
}
