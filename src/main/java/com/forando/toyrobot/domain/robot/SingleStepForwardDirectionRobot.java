package com.forando.toyrobot.domain.robot;

import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.domain.gps.receiver.GpsReceiver;
import com.forando.toyrobot.exception.BehaviorNotImplementedException;
import lombok.Setter;

/**
 * This robot can move only forward with a distance equal 1 step
 */
@Setter
public class SingleStepForwardDirectionRobot extends GpsReceiver implements SingleStepRobot {

    private final String TAG;

    public SingleStepForwardDirectionRobot() {
        TAG = this.getClass().getCanonicalName();
    }

    @Override
    public void turnLeft() {
        if (getState() == null) {
            return;
        }
        onTurnLeft();
    }

    @Override
    public void turnRight() {
        if (getState() == null) {
            return;
        }
        onTurnRight();
    }

    @Override
    public void stepForward() {
        if (getState() == null) {
            return;
        }
        int distance = getForwardDistanceToEdge();
        if (distance > 0) {
            onGoForward(1);
        }
    }

    @Override
    public void stepBackward() throws BehaviorNotImplementedException {
        throw new BehaviorNotImplementedException("This is " + TAG + " implementation. This robot cannot go backward");
    }

    @Override
    public RobotState getState() {
        return defineState();
    }
}
