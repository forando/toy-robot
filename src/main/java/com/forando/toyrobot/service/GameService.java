package com.forando.toyrobot.service;

import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.domain.gps.transiver.GpsSatellite;
import com.forando.toyrobot.domain.robot.SingleStepRobot;
import com.forando.toyrobot.domain.space.Table;
import org.springframework.stereotype.Service;

@Service
public class GameService {
    private final SingleStepRobot robot;
    private final Table table;

    public GameService(GpsSatellite satellite,
                       SingleStepRobot robot,
                       Table table) {

        this.robot = robot;
        this.table = table;

        this.robot.setConnection(satellite.getConnectionManager());
    }

    public void place(RobotState state) {
        table.validatePosition(state);
        robot.setInitialState(state);
    }

    public void move() {
        robot.stepForward();
    }

    public void left() {
        robot.turnLeft();
    }

    public void right() {
        robot.turnRight();
    }

    public RobotState report() {
        return robot.getState();
    }
}
