package com.forando.toyrobot.domain;

import com.forando.toyrobot.domain.dto.Facing;
import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.domain.gps.transiver.GpsConnectionManager;
import com.forando.toyrobot.domain.gps.transiver.GpsSatellite;
import com.forando.toyrobot.domain.space.Bounds2D;
import com.forando.toyrobot.domain.space.TableBounds;

public final class TestDada {
    public static final int DEFAULT_WIDTH = 5;
    public static final int DEFAULT_LENGTH = 5;
    public static final Position INITIAL_POSITION = new Position(2, 2);
    public static final RobotState INITIAL_STATE = new RobotState(INITIAL_POSITION, Facing.NORTH);

    public static GpsConnectionManager createConnectionManager() {
        Bounds2D bounds = new TableBounds(0, 0, DEFAULT_WIDTH, DEFAULT_LENGTH);
        GpsSatellite satellite = new GpsSatellite(bounds);
        return satellite.getConnectionManager();
    }
}
