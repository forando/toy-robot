package com.forando.toyrobot.domain.gps.receiver;

import com.forando.toyrobot.domain.TestDada;
import com.forando.toyrobot.domain.dto.Facing;
import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.domain.gps.transiver.ClientTracker;
import com.forando.toyrobot.domain.gps.transiver.GpsConnectionManager;
import com.forando.toyrobot.domain.space.Bounds2D;

import static com.forando.toyrobot.domain.TestDada.INITIAL_POSITION;

class TrackerData {

    static GpsTrackerModeEast east;
    static GpsTrackerModeNorth north;
    static GpsTrackerModeWest west;
    static GpsTrackerModeSouth south;


    static TrackerCheckData eastData;
    static TrackerCheckData northData;
    static TrackerCheckData westData;
    static TrackerCheckData southData;

    static  {

        north = new GpsTrackerModeNorth();
        west = new GpsTrackerModeWest();
        south = new GpsTrackerModeSouth();
        east = new GpsTrackerModeEast();
        Switcher switcher = new Switcher(north);

        setTrackerRelations(east, north, south);
        setTrackerRelations(north, west, east);
        setTrackerRelations(west, south, north);
        setTrackerRelations(south, east, west);

        GpsConnectionManager connectionManager = TestDada.createConnectionManager();
        setConnection(connectionManager);

        String id = east.subscribe(INITIAL_POSITION);
        ClientTracker client = connectionManager.getClient(id);
        Bounds2D bounds = connectionManager.getObservedGeometryBounds();

        int steps = 2;

        eastData = TrackerCheckData.builder()
                .id(id)
                .switcher(switcher)
                .facing(Facing.EAST)
                .bounds(bounds)
                .client(client)
                .forwardDistance(3)
                .backwardDistance(2)
                .steps(steps)
                .initialState(new RobotState(INITIAL_POSITION, Facing.EAST))
                .expectedState(
                        new RobotState(
                                new Position(INITIAL_POSITION.getX() + steps, INITIAL_POSITION.getY()),
                                Facing.EAST
                        )
                )
                .build();

        northData = TrackerCheckData.builder()
                .id(id)
                .switcher(switcher)
                .facing(Facing.NORTH)
                .bounds(bounds)
                .client(client)
                .forwardDistance(3)
                .backwardDistance(2)
                .steps(steps)
                .initialState(new RobotState(INITIAL_POSITION, Facing.NORTH))
                .expectedState(
                        new RobotState(
                                new Position(INITIAL_POSITION.getX(), INITIAL_POSITION.getY() + steps),
                                Facing.NORTH
                        )
                )
                .build();

        westData = TrackerCheckData.builder()
                .id(id)
                .switcher(switcher)
                .facing(Facing.WEST)
                .bounds(bounds)
                .client(client)
                .forwardDistance(2)
                .backwardDistance(3)
                .steps(steps)
                .initialState(new RobotState(INITIAL_POSITION, Facing.WEST))
                .expectedState(
                        new RobotState(
                                new Position(INITIAL_POSITION.getX() - steps, INITIAL_POSITION.getY()),
                                Facing.WEST
                        )
                )
                .build();

        southData = TrackerCheckData.builder()
                .id(id)
                .switcher(switcher)
                .facing(Facing.SOUTH)
                .bounds(bounds)
                .client(client)
                .forwardDistance(2)
                .backwardDistance(3)
                .steps(steps)
                .initialState(new RobotState(INITIAL_POSITION, Facing.SOUTH))
                .expectedState(
                        new RobotState(
                                new Position(INITIAL_POSITION.getX(), INITIAL_POSITION.getY() - steps),
                                Facing.SOUTH
                        )
                )
                .build();

    }

    private static void setTrackerRelations(GpsTrackerMode target, GpsTrackerMode left, GpsTrackerMode right) {
        target.setLeftRelation(left);
        target.setRightRelation(right);
    }

    private static void setConnection(GpsConnectionManager connectionManager) {
        east.setConnection(connectionManager);
        north.setConnection(connectionManager);
        west.setConnection(connectionManager);
        south.setConnection(connectionManager);
    }

    static class Switcher extends GpsTrackerSwitcher {

        private GpsTrackerMode trackerMode;

        Switcher(GpsTrackerMode trackerMode) {
            this.trackerMode = trackerMode;
        }

        public GpsTrackerMode getTrackerMode() {
            return trackerMode;
        }

        @Override
        protected void changeMode(GpsTrackerMode newTracker) {
            this.trackerMode = newTracker;
        }
    }

    static class TrackerCheckData {
        private String id;
        private Facing facing;
        private ClientTracker client;
        private int forwardDistance;
        private int backwardDistance;
        private int steps;
        private RobotState initialState;
        private RobotState expectedState;
        private Bounds2D bounds;
        private Switcher switcher;

        private TrackerCheckData(
                String id,
                Facing facing,
                ClientTracker client,
                int forwardDistance,
                int backwardDistance,
                int steps,
                RobotState initialState,
                RobotState expectedState,
                Bounds2D bounds,
                Switcher switcher) {

            this.id = id;
            this.facing = facing;
            this.client = client;
            this.forwardDistance = forwardDistance;
            this.backwardDistance = backwardDistance;
            this.steps = steps;
            this.initialState = initialState;
            this.expectedState = expectedState;
            this.bounds = bounds;
            this.switcher = switcher;
        }

        public String getId() {
            return id;
        }

        public Facing getFacing() {
            return facing;
        }

        public ClientTracker getClient() {
            return client;
        }

        public int getForwardDistance() {
            return forwardDistance;
        }

        public int getBackwardDistance() {
            return backwardDistance;
        }

        public int getSteps() {
            return steps;
        }

        public RobotState getInitialState() {
            return initialState;
        }

        public RobotState getExpectedState() {
            return expectedState;
        }

        public Bounds2D getBounds() {
            return bounds;
        }

        public Switcher getSwitcher() {
            return switcher;
        }

        static TrackerCheckDataBuilder builder() {
            return new TrackerCheckDataBuilder();
        }

        static class TrackerCheckDataBuilder {
            private String id;
            private Facing facing;
            private ClientTracker client;
            private int forwardDistance;
            private int backwardDistance;
            private int steps;
            private RobotState initialState;
            private RobotState expectedState;
            private Bounds2D bounds;
            private Switcher switcher;

            TrackerCheckDataBuilder id(String id) {
                this.id = id;
                return this;
            }

            TrackerCheckDataBuilder facing(Facing facing) {
                this.facing = facing;
                return this;
            }

            TrackerCheckDataBuilder client(ClientTracker client) {
                this.client = client;
                return this;
            }

            TrackerCheckDataBuilder forwardDistance(int forwardDistance) {
                this.forwardDistance = forwardDistance;
                return this;
            }

            TrackerCheckDataBuilder backwardDistance(int backwardDistance) {
                this.backwardDistance = backwardDistance;
                return this;
            }

            TrackerCheckDataBuilder steps(int steps) {
                this.steps = steps;
                return this;
            }

            TrackerCheckDataBuilder initialState(RobotState initialState) {
                this.initialState = initialState;
                return this;
            }

            TrackerCheckDataBuilder expectedState(RobotState expectedState) {
                this.expectedState = expectedState;
                return this;
            }

            TrackerCheckDataBuilder bounds(Bounds2D bounds) {
                this.bounds = bounds;
                return this;
            }

            TrackerCheckDataBuilder switcher(Switcher switcher) {
                this.switcher = switcher;
                return this;
            }

            TrackerCheckData build() {
                return new TrackerCheckData(
                        id, facing, client, forwardDistance, backwardDistance,
                        steps, initialState, expectedState, bounds, switcher
                );
            }
        }
    }

}
