package com.forando.toyrobot.domain.gps.transiver;

import com.forando.toyrobot.domain.space.Bounds2D;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.forando.toyrobot.domain.TestDada.INITIAL_POSITION;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class GpsConnectionManagerTest {

    @Mock
    private Bounds2D bounds;

    private GpsConnectionManager manager;

    @Before
    public void setUp() {
        GpsSatellite satellite = new GpsSatellite(bounds);
        GpsConnectionManager.injectInto(satellite);
        manager = satellite.getConnectionManager();
    }

    @Test
    public void test_subscribeClient() {
        String id = manager.subscribeClient(INITIAL_POSITION);
        assertNotNull(id);
        assertEquals("client size", 1, manager.getClientSize());
    }

    @Test
    public void test_getClient() {
        String id = manager.subscribeClient(INITIAL_POSITION);
        ClientTracker givenBack = manager.getClient(id);
        assertEquals("client position", INITIAL_POSITION, givenBack.getPosition());
        assertEquals("client size", 1, manager.getClientSize());
    }

    @Test
    public void test_unsubscribeClient() {
        String id = manager.subscribeClient(INITIAL_POSITION);
        ClientTracker givenBack = manager.unsubscribeClient(id);
        assertEquals("client position", INITIAL_POSITION, givenBack.getPosition());
        assertEquals("client size", 0, manager.getClientSize());
    }

}