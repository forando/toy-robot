package com.forando.toyrobot.domain.gps.receiver;

import com.forando.toyrobot.domain.TestDada;
import com.forando.toyrobot.domain.dto.Facing;
import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.domain.dto.RobotState;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.forando.toyrobot.domain.TestDada.INITIAL_POSITION;
import static com.forando.toyrobot.domain.TestDada.INITIAL_STATE;

/**
 * <b>DISCLAIMER</b>
 * There is good chance to resize code, but I simply don't have time :)
 */
public class GpsReceiverIntegrationTest {

    private GpsReceiver receiver;

    @Before
    public void setUp() {
        receiver = new GpsReceiver();
        receiver.setConnection(TestDada.createConnectionManager());
        receiver.setInitialState(new RobotState(INITIAL_POSITION, Facing.NORTH));
    }

    @Test
    public void test_onTurnLeft() {
        receiver.onTurnLeft();
        thenCheckState("first call", new RobotState(INITIAL_POSITION, Facing.WEST));
        receiver.onTurnLeft();
        thenCheckState("second call", new RobotState(INITIAL_POSITION, Facing.SOUTH));
        receiver.onTurnLeft();
        thenCheckState("third call", new RobotState(INITIAL_POSITION, Facing.EAST));
        receiver.onTurnLeft();
        thenCheckState("forth call", INITIAL_STATE);
    }

    @Test
    public void test_onTurnRight() {
        receiver.onTurnRight();
        thenCheckState("first call", new RobotState(INITIAL_POSITION, Facing.EAST));
        receiver.onTurnRight();
        thenCheckState("second call", new RobotState(INITIAL_POSITION, Facing.SOUTH));
        receiver.onTurnRight();
        thenCheckState("third call", new RobotState(INITIAL_POSITION, Facing.WEST));
        receiver.onTurnRight();
        thenCheckState("forth call", INITIAL_STATE);
    }

    @Test
    public void test_getForwardDistanceToEdge() {
        Assert.assertEquals("looking NORTH", 3, receiver.getForwardDistanceToEdge());
        receiver.onTurnRight();
        Assert.assertEquals("looking EAST", 3, receiver.getForwardDistanceToEdge());
        receiver.onTurnRight();
        Assert.assertEquals("looking SOUTH", 2, receiver.getForwardDistanceToEdge());
        receiver.onTurnRight();
        Assert.assertEquals("looking WEST", 2, receiver.getForwardDistanceToEdge());
    }

    @Test
    public void test_getBackwardDistanceToEdge() {
        Assert.assertEquals("looking NORTH", 2, receiver.getBackwardDistanceToEdge());
        receiver.onTurnRight();
        Assert.assertEquals("looking EAST", 2, receiver.getBackwardDistanceToEdge());
        receiver.onTurnRight();
        Assert.assertEquals("looking SOUTH", 3, receiver.getBackwardDistanceToEdge());
        receiver.onTurnRight();
        Assert.assertEquals("looking WEST", 3, receiver.getBackwardDistanceToEdge());
    }

    @Test
    public void test_onGoForward() {
        receiver.onGoForward(2);
        thenCheckState(
                "state mismatch",
                new RobotState(
                        new Position(
                                INITIAL_POSITION.getX(),
                                INITIAL_POSITION.getY() + 2
                        ),
                        Facing.NORTH
                )
        );
    }

    @Test
    public void test_turnRight_and_onGoForward() {
        receiver.onTurnRight();
        receiver.onGoForward(2);
        thenCheckState(
                "state mismatch",
                new RobotState(
                        new Position(
                                INITIAL_POSITION.getX() + 2,
                                INITIAL_POSITION.getY()
                        ),
                        Facing.EAST
                )
        );
    }

    @Test
    public void test_turnLeft_and_onGoForward() {
        receiver.onTurnLeft();
        receiver.onGoForward(2);
        thenCheckState(
                "state mismatch",
                new RobotState(
                        new Position(
                                INITIAL_POSITION.getX() - 2,
                                INITIAL_POSITION.getY()
                        ),
                        Facing.WEST
                )
        );
    }

    @Test
    public void test_returnOverLeft_and_onGoForward() {
        receiver.onTurnLeft();
        receiver.onTurnLeft();
        receiver.onGoForward(2);
        thenCheckState(
                "state mismatch",
                new RobotState(
                        new Position(
                                INITIAL_POSITION.getX(),
                                INITIAL_POSITION.getY() - 2
                        ),
                        Facing.SOUTH
                )
        );
    }

    @Test
    public void test_returnOverRight_and_onGoForward() {
        receiver.onTurnRight();
        receiver.onTurnRight();
        receiver.onGoForward(2);
        thenCheckState(
                "state mismatch",
                new RobotState(
                        new Position(
                                INITIAL_POSITION.getX(),
                                INITIAL_POSITION.getY() - 2
                        ),
                        Facing.SOUTH
                )
        );
    }

    @Test
    public void test_onGoBackward() {
        receiver.onGoBackward(2);
        thenCheckState(
                "state mismatch",
                new RobotState(
                        new Position(
                                INITIAL_POSITION.getX(),
                                INITIAL_POSITION.getY() - 2
                        ),
                        Facing.NORTH
                )
        );
    }

    @Test
    public void test_turnRight_and_onGoBackward() {
        receiver.onTurnRight();
        receiver.onGoBackward(2);
        thenCheckState(
                "state mismatch",
                new RobotState(
                        new Position(
                                INITIAL_POSITION.getX() - 2,
                                INITIAL_POSITION.getY()
                        ),
                        Facing.EAST
                )
        );
    }

    @Test
    public void test_turnLeft_and_onGoBackward() {
        receiver.onTurnLeft();
        receiver.onGoBackward(2);
        thenCheckState(
                "state mismatch",
                new RobotState(
                        new Position(
                                INITIAL_POSITION.getX() + 2,
                                INITIAL_POSITION.getY()
                        ),
                        Facing.WEST
                )
        );
    }

    @Test
    public void test_returnOverLeft_and_onGoBackward() {
        receiver.onTurnLeft();
        receiver.onTurnLeft();
        receiver.onGoBackward(2);
        thenCheckState(
                "state mismatch",
                new RobotState(
                        new Position(
                                INITIAL_POSITION.getX(),
                                INITIAL_POSITION.getY() + 2
                        ),
                        Facing.SOUTH
                )
        );
    }

    @Test
    public void test_returnOverRight_and_onGoBackward() {
        receiver.onTurnRight();
        receiver.onTurnRight();
        receiver.onGoBackward(2);
        thenCheckState(
                "state mismatch",
                new RobotState(
                        new Position(
                                INITIAL_POSITION.getX(),
                                INITIAL_POSITION.getY() + 2
                        ),
                        Facing.SOUTH
                )
        );
    }

    private void thenCheckState(String errorMessage,  RobotState expected){
        Assert.assertEquals(errorMessage, expected, receiver.defineState());
    }
}