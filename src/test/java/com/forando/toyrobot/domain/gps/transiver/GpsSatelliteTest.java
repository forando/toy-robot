package com.forando.toyrobot.domain.gps.transiver;

import com.forando.toyrobot.domain.space.Bounds2D;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GpsSatelliteTest {

    @Mock
    private Bounds2D bounds;

    @Test
    public void test_getConnectionManager() {
        GpsSatellite satellite = new GpsSatellite(bounds);
        Assert.assertNotNull(satellite.getConnectionManager());
    }
}
