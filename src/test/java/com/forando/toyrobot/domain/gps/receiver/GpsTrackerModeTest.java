package com.forando.toyrobot.domain.gps.receiver;

import com.forando.toyrobot.domain.dto.Facing;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

import java.util.Arrays;

import static com.forando.toyrobot.domain.TestDada.INITIAL_POSITION;
import static com.forando.toyrobot.domain.gps.receiver.TrackerData.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(Parameterized.class)
public class GpsTrackerModeTest {

    @Parameterized.Parameters(name = "{index}: case({0}, {1})")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { east, eastData }, { north, northData }, { west, westData }, { south, southData }
        });
    }

    @Parameter
    public GpsTrackerMode tracker;

    @Parameter(1)
    public TrackerCheckData checkData;

    @Test
    public void test_getState() {
        assertEquals(INITIAL_POSITION, tracker.getState(checkData.getId()).getPosition());
    }

    @Test
    public void test_getFacing() {
        assertEquals(checkData.getFacing(), tracker.getFacing());
    }

    @Test
    public void test_getForwardDistance() {
        assertEquals(checkData.getForwardDistance(), tracker.getForwardDistanceToEdge(checkData.getId()));
    }

    @Test
    public void test_getBackwardDistance() {
        assertEquals(checkData.getBackwardDistance(), tracker.getBackwardDistanceToEdge(checkData.getId()));
    }

    @Test
    public void test_goForward_Backward() {
        assertEquals(
                checkData.getExpectedState(),
                tracker.goForward(checkData.getId(), checkData.getSteps())
        );

        assertEquals(
                checkData.getInitialState(),
                tracker.goBackward(checkData.getId(), checkData.getSteps())
        );
    }

    @Test
    public void test_turnLeft() {
        Switcher switcher = checkData.getSwitcher();
        switcher.changeMode(tracker);

        Facing original = switcher.getTrackerMode().getFacing();

        tracker.turnLeft(switcher);

        assertNotEquals(original, switcher.getTrackerMode().getFacing());
    }

    @Test
    public void test_turnRight() {
        Switcher switcher = checkData.getSwitcher();
        switcher.changeMode(tracker);

        Facing original = switcher.getTrackerMode().getFacing();

        tracker.turnRight(switcher);

        assertNotEquals(original, switcher.getTrackerMode().getFacing());
    }
}
