package com.forando.toyrobot.domain.gps.transiver;

import com.forando.toyrobot.domain.dto.Position;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ClientTrackerTest {

    private static final int INIT_X = 2;
    private static final int INIT_Y = 2;

    private ClientTracker clientTracker = new ClientTracker("id", new Position(2, 2));

    private Position input;

    @Before
    public void setUp() {
        input = new Position(INIT_X, INIT_Y);
        //todo: change
        //clientTracker.setPosition(input);
    }

    @Test
    public void test_setPosition() {
        Assert.assertEquals(input, clientTracker.getPosition());
    }

    @Test
    public void test_appendToX() {
        clientTracker.appendToX(1);
        Position expected = new Position(INIT_X + 1, INIT_Y);
        Assert.assertEquals(expected, clientTracker.getPosition());
    }

    @Test
    public void test_appendToY() {
        clientTracker.appendToY(1);
        Position expected = new Position(INIT_X, INIT_Y + 1);
        Assert.assertEquals(expected, clientTracker.getPosition());
    }

    @Test
    public void test_deductFromX() {
        clientTracker.deductFromX(1);
        Position expected = new Position(INIT_X - 1, INIT_Y);
        Assert.assertEquals(expected, clientTracker.getPosition());
    }

    @Test
    public void test_deductFromY() {
        clientTracker.deductFromY(1);
        Position expected = new Position(INIT_X, INIT_Y - 1);
        Assert.assertEquals(expected, clientTracker.getPosition());
    }
}
