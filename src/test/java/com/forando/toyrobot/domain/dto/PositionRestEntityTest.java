package com.forando.toyrobot.domain.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.forando.toyrobot.controller.entity.PositionRestEntity;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class PositionRestEntityTest {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Test
    public void test_marshal_state() throws IOException {
        PositionRestEntity input = new PositionRestEntity(2, 2);
        String stringValue = MAPPER.writeValueAsString(input);

        InputStream stream = new ByteArrayInputStream(stringValue.getBytes(StandardCharsets.UTF_8));

        PositionRestEntity output = MAPPER.readValue(stream, PositionRestEntity.class);

        Assert.assertEquals(input, output);
    }
}
