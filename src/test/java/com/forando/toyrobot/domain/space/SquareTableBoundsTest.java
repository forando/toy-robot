package com.forando.toyrobot.domain.space;

import com.forando.toyrobot.domain.dto.Position;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SquareTableBoundsTest {

    @Test(expected = IllegalArgumentException.class)
    public void cannot_create_bounds_with_zero_width() {
        new TableBounds(0, 0, 0, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void cannot_create_bounds_with_zero_length() {
        new TableBounds(0, 0, 1, 0);
    }

    @Test
    public void test_getStartPosition() {
        TableBounds bounds = new TableBounds(0, 0, 1, 1);
        assertEquals("startPosition", new Position(0, 0), bounds.getOrigin());
    }

    @Test
    public void test_getStartX() {
        TableBounds bounds = new TableBounds(2, 0, 2, 3);
        assertEquals("startX", 2, bounds.getStartX());
    }

    @Test
    public void test_getStartY() {
        TableBounds bounds = new TableBounds(0, 3, 2, 3);
        assertEquals("startX", 3, bounds.getStartY());
    }

    @Test
    public void test_getWidth() {
        TableBounds bounds = new TableBounds(0, 0, 2, 3);
        assertEquals("width", 2, bounds.getWidth());
    }

    @Test
    public void test_getLength() {
        TableBounds bounds = new TableBounds(0, 0, 2, 3);
        assertEquals("length", 3, bounds.getLength());
    }
}
