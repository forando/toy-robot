package com.forando.toyrobot.domain.space;

import com.forando.toyrobot.domain.dto.Facing;
import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.domain.dto.RobotState;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SquareTableTest {

    private static final int START_X = 2;
    private static final int START_Y = 2;

    @Mock
    private TableBounds bounds;

    private SquareTable squareTable;

    private Facing initialFacing = Facing.NORTH;

    private Position initialPosition;

    private RobotState initialState;

    @Before
    public void setUp() {
        doNothing().when(bounds).validatePosition(any(Position.class));
        when(bounds.getLength()).thenReturn(5);
        when(bounds.getWidth()).thenReturn(5);
        when(bounds.getOrigin()).thenReturn(new Position(0, 0));

        initialPosition = new Position(START_X, START_Y);
        initialState = new RobotState(initialPosition, initialFacing);

        squareTable = new SquareTable(bounds);
    }

    @Test
    public void test_getWidth() {
        squareTable.getWidth();
        verify(bounds).getWidth();
        verifyNoMoreInteractions(bounds);
    }

    @Test
    public void test_getLength() {
        squareTable.getLength();
        verify(bounds).getLength();
        verifyNoMoreInteractions(bounds);
    }

    @Test
    public void test_getOrigin() {
        squareTable.getStart();
        verify(bounds).getOrigin();
        verifyNoMoreInteractions(bounds);
    }

    @Test
    public void test_getEnd() {
        squareTable.getEnd();
        verify(bounds).getStartX();
        verify(bounds).getStartY();
        verify(bounds).getWidth();
        verify(bounds).getLength();
        verifyNoMoreInteractions(bounds);
    }

    @Test
    public void test_validatePosition() {
        squareTable.validatePosition(initialState);
        verify(bounds).validatePosition(initialPosition);
        verifyNoMoreInteractions(bounds);
    }
}
