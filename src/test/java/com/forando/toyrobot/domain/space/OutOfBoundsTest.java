package com.forando.toyrobot.domain.space;

import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.exception.PositionOutOfBoundsException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class OutOfBoundsTest {
    @Parameters(name = "{index}: case({0}, {1})")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { -100, -100 }, { -1, -1 }, { -1, 0 }, { 0, -1 }, { 6, 5 }, { 5, 6 }, { 6, 6 }, { 100, 100 }
        });
    }

    private TableBounds bounds;

    @Parameter
    public int inputX;

    @Parameter(1)
    public int inputY;
    @Before
    public void setUp() {
        bounds = new TableBounds(0, 0, 5, 5);
    }


    @Test(expected = PositionOutOfBoundsException.class)
    public void test_validatePosition_with_invalid_input() {
        bounds.validatePosition(new Position(inputX, inputY));
    }

    @Test
    public void test_isWithinBounds_with_invalid_input() {
        Assert.assertFalse(bounds.isWithinBounds(new Position(inputX, inputY)));
    }
}
