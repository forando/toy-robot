package com.forando.toyrobot.domain.space;

import com.forando.toyrobot.domain.dto.Position;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class InBoundsTest {
    @Parameters(name = "{index}: case({0}, {1})")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { 0, 0 }, { 0, 1 }, { 1, 0 }, { 2, 2 }, { 4, 5 }, { 5, 4 }, { 0, 5 }, { 5, 0 }
        });
    }

    private TableBounds bounds;

    @Parameter
    public int inputX;

    @Parameter(1)
    public int inputY;

    @Before
    public void setUp() {
        bounds = new TableBounds(0, 0, 5, 5);
    }

    @Test
    public void test_validatePosition_with_valid_input() {
        bounds.validatePosition(new Position(inputX, inputY));
    }

    @Test
    public void test_isWithinBounds_with_valid_input() {
        Position position = new Position(inputX, inputY);
        Assert.assertTrue(bounds.isWithinBounds(position));
    }
}
