package com.forando.toyrobot.domain.robot;

import com.forando.toyrobot.domain.TestDada;
import com.forando.toyrobot.domain.dto.Facing;
import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.exception.BehaviorNotImplementedException;
import org.junit.Before;
import org.junit.Test;

import static com.forando.toyrobot.domain.TestDada.INITIAL_POSITION;
import static com.forando.toyrobot.domain.TestDada.INITIAL_STATE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SingleStepForwardDirectionRobotIntegrationTest {

    private SingleStepForwardDirectionRobot robot;

    @Before
    public void setUp() {
        robot = new SingleStepForwardDirectionRobot();
        robot.setConnection(TestDada.createConnectionManager());
    }

    @Test
    public void test_getState() {
        robot.setInitialState(INITIAL_STATE);
        assertEquals(INITIAL_STATE, robot.getState());
    }

    @Test
    public void test_getState_without_initialState_returns_null() {
        assertNull(robot.getState());
    }

    @Test
    public void test_reset_connection_preserves_current_state() {
        robot.setInitialState(INITIAL_STATE);
        robot.stepForward();
        robot.stepForward();
        robot.turnRight();
        robot.stepForward();
        RobotState state = robot.getState();

        robot.setConnection(TestDada.createConnectionManager());

        assertEquals(state, robot.getState());
    }

    @Test
    public void test_turnLeft() {
        robot.setInitialState(INITIAL_STATE);
        RobotState expected = new RobotState(INITIAL_STATE.getPosition(), Facing.WEST);
        robot.turnLeft();
        assertEquals(expected, robot.getState());
   }

    @Test
    public void test_turnRight() {
        robot.setInitialState(INITIAL_STATE);
        RobotState expected = new RobotState(INITIAL_STATE.getPosition(), Facing.EAST);
        robot.turnRight();
        assertEquals(expected, robot.getState());
    }

    @Test
    public void test_stepForward_success() {
        robot.setInitialState(INITIAL_STATE);
        RobotState expected = new RobotState(
                new Position(INITIAL_POSITION.getX(), INITIAL_POSITION.getY() + 1), Facing.NORTH
        );
        robot.stepForward();
        assertEquals(expected, robot.getState());
    }

    @Test
    public void test_stepForward_forbidden() {
        RobotState initialState = new RobotState(new Position(2, 5), Facing.NORTH);
        robot.setInitialState(initialState);
        robot.stepForward();
        assertEquals(initialState, robot.getState());
    }

    @Test
    public void test_stepForward_ignored_if_no_state() {
        robot.stepForward();
        assertNull(robot.getState());
    }

    @Test
    public void test_turnLeft_ignored_if_no_state() {
        robot.turnLeft();
        assertNull(robot.getState());
    }

    @Test
    public void test_turnRight_ignored_if_no_state() {
        robot.turnRight();
        assertNull(robot.getState());
    }

    @Test(expected = BehaviorNotImplementedException.class)
    public void test_stepBackward_not_used() throws BehaviorNotImplementedException {
        robot.stepBackward();
    }
}
