package com.forando.toyrobot.controller;

import com.forando.toyrobot.controller.entity.Command;
import com.forando.toyrobot.controller.entity.PositionRestEntity;
import com.forando.toyrobot.controller.entity.RequestRestEntity;
import com.forando.toyrobot.controller.entity.StateRestEntity;
import com.forando.toyrobot.domain.dto.Facing;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GameControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;


    @Test
    public void test_post_place_valid_input() {
        ResponseEntity<Object> responseEntity = placeInput(Command.PLACE,2);

        assertEquals(204, responseEntity.getStatusCodeValue());
    }

    private ResponseEntity<Object> placeInput(Command command, int y) {
        RequestRestEntity requestRestEntity = new RequestRestEntity(command, 2, y, Facing.NORTH);

        HttpEntity<RequestRestEntity> request = new HttpEntity<>(requestRestEntity);

        return this.restTemplate.postForEntity("http://localhost:" + port + "/" + "toy-robot",
                request, Object.class);
    }

    @Test
    public void test_post_report() {
        placeInput(Command.PLACE,2);
        RequestRestEntity requestRestEntity = new RequestRestEntity();
        requestRestEntity.setCommand(Command.REPORT);

        HttpEntity<RequestRestEntity> request = new HttpEntity<>(requestRestEntity);

        ResponseEntity<StateRestEntity> responseEntity =
                this.restTemplate.postForEntity("http://localhost:" + port + "/" + "toy-robot",
                        request, StateRestEntity.class);

        assertEquals(200, responseEntity.getStatusCodeValue());

        StateRestEntity stateRestEntity = responseEntity.getBody();
        assertEquals("x", 2, stateRestEntity.getPosition().getX());
        assertEquals("y", 2, stateRestEntity.getPosition().getY());
        assertEquals("facing", Facing.NORTH, stateRestEntity.getFacing());
    }

    @Test
    public void test_post_move() {
        ResponseEntity<Object> responseEntity = placeInput(Command.MOVE,2);

        assertEquals(204, responseEntity.getStatusCodeValue());
    }

    @Test
    public void test_post_left() {
        ResponseEntity<Object> responseEntity = placeInput(Command.LEFT,2);

        assertEquals(204, responseEntity.getStatusCodeValue());
    }

    @Test
    public void test_post_right() {
        ResponseEntity<Object> responseEntity = placeInput(Command.RIGHT,2);

        assertEquals(204, responseEntity.getStatusCodeValue());
    }

    @Test
    public void test_post_place_invalid_json() {
        StateRestEntity robotState  = new StateRestEntity(new PositionRestEntity(2, 11), Facing.NORTH);

        HttpEntity<StateRestEntity> request = new HttpEntity<>(robotState);

        ResponseEntity<Object> responseEntity = restTemplate.postForEntity(
                "http://localhost:" + port + "/" + "toy-robot", request, Object.class
        );

        assertEquals(400, responseEntity.getStatusCodeValue());
    }

    @Test
    public void test_post_place_invalid_position() {
        ResponseEntity<Object> responseEntity = placeInput(Command.PLACE,12);

        assertEquals(403, responseEntity.getStatusCodeValue());
    }

    @Test
    public void test_post_place_command_missing() {
        RequestRestEntity requestRestEntity = new RequestRestEntity();
        requestRestEntity.setFacing(Facing.NORTH);
        requestRestEntity.setY(2);
        requestRestEntity.setX(2);

        HttpEntity<RequestRestEntity> request = new HttpEntity<>(requestRestEntity);

        ResponseEntity<Object> responseEntity = restTemplate.postForEntity(
                "http://localhost:" + port + "/" + "toy-robot", request, Object.class
        );

        assertEquals(400, responseEntity.getStatusCodeValue());
    }
}
