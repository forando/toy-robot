package com.forando.toyrobot.controller;

import com.forando.toyrobot.controller.entity.Command;
import com.forando.toyrobot.controller.entity.RequestRestEntity;
import com.forando.toyrobot.controller.exception.RequestValidationException;
import com.forando.toyrobot.domain.dto.Facing;
import org.junit.Test;

public class RequestValidatorTest {

    private static final RequestRestEntity VALID_ENTITY = new RequestRestEntity(Command.PLACE, 2, 2, Facing.NORTH);

    @Test
    public void test_validateCommand_success() {
        RequestValidator.validateCommand(VALID_ENTITY);
    }

    @Test
    public void test_validateState_success() {
        RequestValidator.validateState(VALID_ENTITY);
    }

    @Test(expected = RequestValidationException.class)
    public void test_validateCommand_fails() {
        RequestRestEntity requestRestEntity = new RequestRestEntity();
        RequestValidator.validateCommand(requestRestEntity);
    }

    @Test(expected = RequestValidationException.class)
    public void test_validateState_no_facing_fails() {
        RequestRestEntity requestRestEntity = new RequestRestEntity();
        requestRestEntity.setX(2);
        requestRestEntity.setY(2);
        RequestValidator.validateState(requestRestEntity);
    }

    @Test(expected = RequestValidationException.class)
    public void test_validateState_no_x_fails() {
        RequestRestEntity requestRestEntity = new RequestRestEntity();
        requestRestEntity.setFacing(Facing.NORTH);
        requestRestEntity.setY(2);
        RequestValidator.validateState(requestRestEntity);
    }

    @Test(expected = RequestValidationException.class)
    public void test_validateState_no_y_fails() {
        RequestRestEntity requestRestEntity = new RequestRestEntity();
        requestRestEntity.setFacing(Facing.NORTH);
        requestRestEntity.setX(2);
        RequestValidator.validateState(requestRestEntity);
    }
}
