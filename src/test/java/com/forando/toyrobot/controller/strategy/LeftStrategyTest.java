package com.forando.toyrobot.controller.strategy;

import com.forando.toyrobot.controller.entity.RequestRestEntity;
import com.forando.toyrobot.service.GameService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LeftStrategyTest {

    @Mock
    private GameService service;

    @Mock
    private RequestRestEntity entity;



    @Test
    public void test_strategy() {
        Strategy strategy = new LeftStrategy();
        strategy.execute(service, entity);
        verify(service).left();
        verifyNoMoreInteractions(service);
        verifyZeroInteractions(entity);
    }

}
