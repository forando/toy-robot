package com.forando.toyrobot.controller.strategy;

import com.forando.toyrobot.controller.entity.RequestRestEntity;
import com.forando.toyrobot.service.GameService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RightStrategyTest {

    @Mock
    private GameService service;

    @Mock
    private RequestRestEntity entity;

    

    @Test
    public void test_strategy() {
        Strategy strategy = new RightStrategy();
        strategy.execute(service, entity);
        verify(service).right();
        verifyNoMoreInteractions(service);
        verifyZeroInteractions(entity);
    }

}
