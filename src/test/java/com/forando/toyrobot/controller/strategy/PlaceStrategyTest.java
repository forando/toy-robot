package com.forando.toyrobot.controller.strategy;

import com.forando.toyrobot.controller.entity.Command;
import com.forando.toyrobot.controller.entity.RequestRestEntity;
import com.forando.toyrobot.controller.exception.RequestValidationException;
import com.forando.toyrobot.domain.dto.Facing;
import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.service.GameService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class PlaceStrategyTest {

    @Mock
    private GameService service;

    private RequestRestEntity validEntity = new RequestRestEntity(Command.PLACE, 2, 2, Facing.NORTH);
    private RobotState initialState = new RobotState(new Position(2, 2), Facing.NORTH);
    

    @Test
    public void test_strategy_with_valid_entity() {
        Strategy strategy = new PlaceStrategy();
        strategy.execute(service, validEntity);
        verify(service).place(initialState);
        verifyNoMoreInteractions(service);
    }

    @Test(expected = RequestValidationException.class)
    public void test_strategy_with_invalid_entity() {
        RequestRestEntity entity = new RequestRestEntity();
        entity.setCommand(Command.PLACE);
        Strategy strategy = new PlaceStrategy();
        strategy.execute(service, entity);
    }

}
