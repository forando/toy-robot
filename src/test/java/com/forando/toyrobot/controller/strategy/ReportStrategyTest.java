package com.forando.toyrobot.controller.strategy;

import com.forando.toyrobot.controller.entity.PositionRestEntity;
import com.forando.toyrobot.controller.entity.RequestRestEntity;
import com.forando.toyrobot.controller.entity.StateRestEntity;
import com.forando.toyrobot.domain.dto.Facing;
import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.service.GameService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ReportStrategyTest {

    @Mock
    private GameService service;

    @Mock
    private RequestRestEntity entity;

    private RobotState state = new RobotState(new Position(2, 2), Facing.NORTH);



    @Test
    public void test_strategy_with_valid_entity() {
        when(service.report()).thenReturn(state);
        Strategy strategy = new ReportStrategy();

        StateRestEntity stateRestEntity = new StateRestEntity(
                new PositionRestEntity(state.getPosition().getX(), state.getPosition().getY()), state.getFacing()
        );
        ResponseEntity<StateRestEntity> expected = new ResponseEntity<>(stateRestEntity, HttpStatus.OK);

        ResponseEntity<?> responseEntity = strategy.execute(service, entity);
        Assert.assertEquals(expected, responseEntity);
        verify(service).report();
        verifyNoMoreInteractions(service);
        verifyZeroInteractions(entity);
    }

}
