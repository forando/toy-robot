package com.forando.toyrobot.controller.strategy;

import com.forando.toyrobot.controller.entity.Command;
import org.junit.Assert;
import org.junit.Test;

public class StrategyMapperTest {

    @Test
    public void test_place_mapping() {
        Strategy strategy = StrategyMapper.fromCommand(Command.PLACE);

        Assert.assertTrue(strategy instanceof PlaceStrategy);
    }

    @Test
    public void test_report_mapping() {
        Strategy strategy = StrategyMapper.fromCommand(Command.REPORT);

        Assert.assertTrue(strategy instanceof ReportStrategy);
    }

    @Test
    public void test_move_mapping() {
        Strategy strategy = StrategyMapper.fromCommand(Command.MOVE);

        Assert.assertTrue(strategy instanceof MoveStrategy);
    }

    @Test
    public void test_left_mapping() {
        Strategy strategy = StrategyMapper.fromCommand(Command.LEFT);

        Assert.assertTrue(strategy instanceof LeftStrategy);
    }

    @Test
    public void test_right_mapping() {
        Strategy strategy = StrategyMapper.fromCommand(Command.RIGHT);

        Assert.assertTrue(strategy instanceof RightStrategy);
    }
}
