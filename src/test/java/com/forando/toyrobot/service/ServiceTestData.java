package com.forando.toyrobot.service;


import com.forando.toyrobot.domain.dto.Facing;
import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.domain.dto.RobotState;

final class ServiceTestData {
    private ServiceTestData() {}

    static final int TEST_WIDTH = 10;
    static final int TEST_LENGTH = 10;

    static final RobotState VALID_STATE = new RobotState(new Position(TEST_WIDTH, TEST_LENGTH), Facing.NORTH);
    static final RobotState INVALID_STATE = new RobotState(new Position(TEST_WIDTH + 1, TEST_LENGTH + 1), Facing.NORTH);
}
