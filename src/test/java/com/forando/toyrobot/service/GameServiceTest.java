package com.forando.toyrobot.service;

import com.forando.toyrobot.domain.dto.Facing;
import com.forando.toyrobot.domain.dto.Position;
import com.forando.toyrobot.domain.dto.RobotState;
import com.forando.toyrobot.exception.PositionOutOfBoundsException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static com.forando.toyrobot.service.ServiceTestData.INVALID_STATE;
import static com.forando.toyrobot.service.ServiceTestData.VALID_STATE;

@SpringBootTest
@RunWith(SpringRunner.class)
public class GameServiceTest {

    @Autowired
    private GameService service;


    @Test
    public void test_place_valid_state_and_get() {
        service.place(VALID_STATE);
        Assert.assertEquals(VALID_STATE, service.report());
    }

    @Test(expected = PositionOutOfBoundsException.class)
    public void test_place_invalid_state() {
        service.place(INVALID_STATE);
    }

    @Test
    public void test_movements() {
        RobotState initialState = new RobotState(new Position(1, 2), Facing.EAST);
        service.place(initialState);
        service.move();
        service.left();
        service.move();
        service.left();
        service.move();
        service.right();
        RobotState expectedState = new RobotState(new Position(1, 3), Facing.NORTH);
        Assert.assertEquals(expectedState, service.report());
    }
}
